import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { Routes, RouterModule } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { ModalModule, AlertModule, BsDropdownModule } from 'ngx-bootstrap'
import { ToastrModule } from 'ngx-toastr'

import { TableService } from '../services/integrated/table.service'

import { BackendComponent } from './backend.component'
import { DashboardComponent } from './dashboard/dashboard.component'
import { HeaderComponent } from './modules/header/header.component'
import { FooterComponent } from './modules/footer/footer.component'
import { AsidebarComponent } from './modules/asidebar/asidebar.component'
import { AlertComponent } from './modules/alert/alert.component'
import { TagsService } from '../services/integrated/tags.service'
import { uploadFileService } from '../services/integrated/upload.service'
import { LinkService } from '../services/integrated/link.service'

const appRoutes: Routes = [
    {
        path: '',
        component: BackendComponent,
        children: [
            { path: '', redirectTo: 'dashboard' },
            { path: 'dashboard', component: DashboardComponent },
            { path: 'pages', loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule) },
            { path: 'contents', loadChildren: () => import('./contents/contents.module').then(m => m.ContentsModule) },
            { path: 'contacts', loadChildren: () => import('./contact/contact.module').then(m => m.ContactModule) },
            { path: 'user', loadChildren: () => import('./personal/personal.module').then(m => m.PersonalModule) },
            { path: 'settings', loadChildren: () => import('./setting/setting.module').then(m => m.SettingModule) },
            { path: 'service', loadChildren: () => import('./service/service.module').then(m => m.ServiceModule) }
        ]
    }
]
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ToastrModule,
        RouterModule.forChild(appRoutes),
        ModalModule.forRoot(),
        AlertModule.forRoot(),
        BsDropdownModule.forRoot(),
        TranslateModule
    ],
    exports: [RouterModule],
    declarations: [AsidebarComponent, BackendComponent, DashboardComponent, HeaderComponent, FooterComponent, AlertComponent],

    providers: [uploadFileService, TagsService, LinkService, TableService],

    entryComponents: [AlertComponent]
})
export class BackendModule {}
