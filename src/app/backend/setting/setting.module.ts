import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { SettingComponent } from './setting.component'
import { Routes, RouterModule } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AlertModule } from 'ngx-bootstrap/alert'
import { EmailComponent } from './email/email.component'
import { WebsiteComponent } from './website/website.component'
import { TagsService } from '../../services/integrated/tags.service'
import { OrtherComponent } from './orther/orther.component'
import { TimepickerModule } from 'ngx-bootstrap/timepicker'
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker'
import { CKEditorModule } from 'ckeditor4-angular'

export const routes: Routes = [
    {
        path: '',
        component: SettingComponent,
        children: [
            { path: '', redirectTo: 'website' },
            { path: 'website', component: WebsiteComponent },
            { path: 'menu', loadChildren: () => import('./menu/menu.module').then(m => m.SettingsMenuModule) },
            { path: 'email', component: EmailComponent },
            { path: 'slide', loadChildren: () => import('./slide/slide.module').then(m => m.SlideModule) },
            { path: 'company', component: OrtherComponent },
            { path: 'link', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            { path: 'content', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            { path: 'social-network', component: OrtherComponent },
            { path: 'other', component: OrtherComponent },
        ]
    }
]
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        AlertModule.forRoot(),
        TimepickerModule.forRoot(),
        BsDatepickerModule.forRoot(),
        CKEditorModule
    ],
    providers: [EmailComponent, WebsiteComponent, OrtherComponent, TagsService],
    declarations: [SettingComponent, EmailComponent, WebsiteComponent, OrtherComponent]
})
export class SettingModule {}
