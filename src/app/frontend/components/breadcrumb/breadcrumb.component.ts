import { Component, Input, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Globals } from '../../../globals'
import { handleCodeRouter } from '../../core/constants/store'

@Component({
    selector: 'breadcrumb-component',
    templateUrl: './breadcrumb.component.html'
})
export class BreadcrumbComponent implements OnInit {
    @Input('input') data
    language: string = ''

    constructor(public globals: Globals, private router: Router) {
        this.language = this.globals.language.getCode().toString() + '/'
    }

    ngOnInit() {}

    handleClick = data => {
        if (handleCodeRouter(data.parent_code)) return

        this.router.navigate(['/' + this.language + data.parent_link])
    }
}
