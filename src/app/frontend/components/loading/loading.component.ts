import { Component, OnInit } from '@angular/core'
import { LOADING_CIRCLE, LOGO } from '../../core/constants'

@Component({
    selector: 'loading-component',
    templateUrl: './loading.component.html',
    styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {
    logo = LOGO
    loadingCircle = LOADING_CIRCLE

    constructor() {}

    ngOnInit() {}
}
