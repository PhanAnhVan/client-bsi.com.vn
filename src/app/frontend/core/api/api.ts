// App Component
export const GET_COMPANY_INFO = 'api/company'
export const GetCompanyInfo = '@Company/GetCompanyInfo'

// Pages
export const GET_CHILDREN_PAGES = 'api/pages/getChildrenPages'
export const GetChildrenPages = '@Pages/GetAllChildren'
export const PAGES_GET_HOT_NEWS = 'api/pages/getHotNews'
export const PagesGetHotNews = '@News/GetHot'
export const PAGES_GET_BANNER = 'api/pages/getBanner'
export const PagesGetBanner = '@Banner/GetBanner'
