import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core'
import { Router } from '@angular/router'
import { Globals } from '../../../../globals'

@Component({
    selector: 'app-menu-mobile',
    templateUrl: './menu-mobile.component.html',
    styleUrls: ['./menu-mobile.component.scss']
})
export class MenuMobileComponent implements OnInit, OnDestroy {
    @Output('menumobile') menumobile = new EventEmitter<boolean>()

    public connect

    public show: number

    public url: string = ''

    public data: any

    public menu: any

    private token = { menu: 'api/getmenu' }

    constructor(public globals: Globals, public router: Router) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getMenu':
                    let data = this.compaid(res.data)
                    this.menu = data
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.globals.send({
            path: this.token.menu,
            token: 'getMenu',
            params: { position: 'menuMain' }
        })
    }

    compaid(data) {
        let list = []

        data = data.filter(function (item) {
            let v = isNaN(+item.parent_id) && item.parent_id ? 0 : +item.parent_id
            v == 0 ? '' : list.push(item)
            return v == 0 ? true : false
        })

        let compaidmenu = (data, skip, level = 0) => {
                level = level + 1

                if (skip == true) {
                return data
            } else {
                for (let i = 0; i < data.length; i++) {
                    let obj = []
                    list = list.filter(item => {
                        let skip = +item.parent_id == +data[i]['id'] ? false : true
                        if (skip == false) {
                            obj.push(item)
                        }
                        return skip
                    })
                    let skip = obj.length == 0 ? true : false

                    data[i]['level'] = level
                    data[i]['href'] = getType(data[i]['link'], +data[i].type)
                    // data[i]['data'] = compaidmenu(obj, skip)
                    data[i]['data'] = level >= 2 ? [] : compaidmenu(obj, skip, level)
                }
                return data
            }
        }

        let getType = (link, type) => {
            switch (+type) {
                case 1:
                case 2:
                case 3:
                case 4:
                    link = link
                    break
                default:
                    break
            }
            return link
        }
        return compaidmenu(data, false)
    }

    handleItemClick(skip: boolean, item: { id: string; data: []; href: string; code: string, level: number }, router = false) {
        const navItem = document.getElementById('dropdown-menu-child-' + item.id)
        const navItemAll = document.querySelectorAll('.active-menu-child')
        const navContainer = document.getElementById('menu-mobi')

        if (item.data && item.data.length > 0 && !router) {
            skip ? navItem.classList.add('active-menu-child') : navItem.classList.remove('active-menu-child')
        } else {
            for (let i = 0; i < navItemAll.length; i++) {
                navItemAll[i].classList.remove('active-menu-child')
            }

            // const CODE_LIST = ['resources', 'company']
            // if (CODE_LIST.includes(item.code)) return

            // console.log("🚀 ~ file: menu-mobile.component.ts ~ line 109 ~ MenuMobileComponent ~ handleItemClick ~ index", item)
            if (item.level === 1) return

            navContainer.classList.toggle('menu-mobi-hidden')
            this.router.navigate(['/' + item.href])

            this.menumobile.emit(false)
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }
}
