import { Component, OnDestroy, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Globals } from '../../../../globals'

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
    // providers: [ToslugService]
})
export class MenuComponent implements OnInit, OnDestroy {
    // @Output('menuStatus') menuStatus = new EventEmitter()
    // @ViewChild('searchElement') searchElement

    private connect
    width: number = innerWidth
    // public itemActive: number = 1
    // public statusOnMobile: boolean = true
    toggleDropdown: number
    isShow: boolean = false
    screenTabletMedium = 991
    isToggleSearch: boolean = false

    constructor(public globals: Globals, public router: Router) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case '@Menu/GetHeader':
                    if (res.status === 1) {
                        const { data } = res
                        const result = this.menu.compared(data)

                        return (this.menu.data = result)
                    }
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.menu.send()
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    menu = {
        data: <any>[],
        send: () => {
            this.globals.send({
                path: 'api/getmenu',
                token: '@Menu/GetHeader',
                params: { position: 'menuMain' }
            })
        },
        compared: (data: any[]) => {
            let list: any[] = []
            data = data.filter(function (item: { parent_id: string | number }) {
                let v = isNaN(+item.parent_id) && item.parent_id ? 0 : +item.parent_id
                v == 0 ? '' : list.push(item)
                return v == 0 ? true : false
            })
            let compare = (data: string | any[], skip: boolean, level = 0) => {
                level = level + 1
                if (skip == true) {
                    return data
                } else {
                    for (let i = 0; i < data.length; i++) {
                        let obj = data[i]['data'] && data[i]['data'].length > 0 ? data[i]['data'] : []
                        list = list.filter(item => {
                            let skip = +item.parent_id == +data[i]['id'] ? false : true
                            if (skip == false) {
                                obj.push(item)
                            }
                            return skip
                        })
                        let skip = obj.length == 0 ? true : false
                        data[i]['level'] = level
                        // data[i]['href'] = data[i]['link']
                        data[i]['data'] = level >= 2 ? [] : compare(obj, skip, level)
                    }
                    return data
                }
            }
            return compare(data, false)
        }
    }

    // _handleCloseOnMobile = () => this.menuStatus.emit(!1)

    // showItem = item => {
    //     this.itemActive = this.itemActive != item.id ? item.id : -1
    // }

    // routerLink = item => {
    //     this.router.navigate(['/' + item.href]), this.menuStatus.emit(!1)
    // }

    // routerLinkUser = link => {
    //     this.menuStatus.emit(!1), this.router.navigate(['/' + link])
    // }

    // infoUser = () => {
    //     this.menuStatus.emit(!1), this.router.navigate(['/user/hoc-vien'])
    // }

    // logout() {
    //     this.globals.CUSTOMER.remove(true)
    //     this.router.navigate(['/'])
    // }

    handleShow = () => (this.isShow = !this.isShow)

    handleHidden = () => {
        this.isShow = false
        this.toggleDropdown = 0
        return
    }

    handleToggleDropdown = (id: number) => (this.toggleDropdown = this.toggleDropdown !== id ? id : 0)

    // handleToggleSearch = () => {
    //     this.isToggleSearch = true

    //     setTimeout(() => {
    //         this.searchElement.nativeElement.focus()
    //     }, 0)

    //     return
    // }

    // search = {
    //     value: '',
    //     onSearch: () => {
    //         // this.search.value = this._toSlug._ini(this.search.value)
    //         const value = this.search.value?.length && this.search.value.trim()
    //         if (!value?.length) return

    //         this.router.navigate(['/tim-kiem'], {
    //             queryParams: { value }
    //         })

    //         this.search.value = ''

    //         return (this.isToggleSearch = false)
    //     }
    // }
}
