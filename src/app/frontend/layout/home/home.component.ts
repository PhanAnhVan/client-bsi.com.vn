import { Component, OnDestroy, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Globals } from '../../../globals'
import { GET_COMPANY_INFO, GetCompanyInfo } from '../../core/api/api'
import { GetStorage, CompanyInfo } from '../../core/constants/store'

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit, OnDestroy {
    private connect
    width: number = innerWidth
    company = <any>{}

    constructor(public globals: Globals, public router: Router) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case '@Home/GetServices':
                    if (res.status === 1) {
                        const { data } = res

                        this.services.data = data
                    }
                    break

                case 'getPagesPin':
                    if (res.status === 1) {
                        const { data } = res

                        this.pages.data = data
                    }
                    break

                case 'getAnalysisHome':
                    if (res.status === 1) {
                        const { data } = res

                        this.analysis.data = data
                    }
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.services.send()

        this.analysis.send()

        this.pages.send()

        // const companyInfo = GetStorage(CompanyInfo) || false
        // if (companyInfo) {
        //     this.company = companyInfo
        // } else {
        //     this.globals.send({
        //         path: GET_COMPANY_INFO,
        //         token: GetCompanyInfo
        //     })
        // }
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    public pages = {
        token: 'api/getPagesPin',
        data: [],
        send: () => {
            this.globals.send({ path: this.pages.token, token: 'getPagesPin' })
        }
    }

    analysis = {
        token: 'api/home/getAnalysis',
        data: <any>{},
        send: () => {
            this.globals.send({
                path: this.analysis.token,
                token: 'getAnalysisHome'
            })
        }
    }

    services = {
        data: <any>{},
        send: () => {
            this.globals.send({
                path: 'api/home/services',
                token: '@Home/GetServices'
            })
        },
        options: {
            items: this.width > 1024 ? 5 : this.width > 425 ? 3 : 2,
            // margin: this.width > 1024 ? 50 : 25,
            // stagePadding: 30,
            loop: true,
            autoplay: false,
            autoplaySpeed: 5000,
            autoplayHoverPause: true,
            dots: false,
            nav: false,
            navText: [
                '<img src="../../../../../assets/img/left-arrow.png" alt="Previous">',
                '<img src="../../../../../assets/img/right-arrow.png" alt="Next">'
            ]
        }
    }
}
