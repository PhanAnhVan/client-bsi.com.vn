import { Component, OnDestroy, OnInit } from '@angular/core'
import { Globals } from '../../../globals'
import { FormBuilder } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'

@Component({
    selector: 'app-detail-page',
    templateUrl: './detail-page.component.html',
    styleUrls: ['./detail-page.component.scss']
})
export class DetailPageComponent implements OnInit, OnDestroy {
    public connect
    public data = <any>{}
    public link: string = ''
    public page = <any>{}

    width: number = innerWidth
    language: string = ''

    public token: any = {
        getDetail: 'api/pages/detail'
    }
    constructor(public globals: Globals, public fb: FormBuilder, public route: ActivatedRoute, public router: Router) {
        this.language = this.globals.language.getCode().toString() + '/'

        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response.token) {
                case 'getDetail':
                    this.data = response.data

                    setTimeout(() => {
                        this.globals.htmlRender()
                    }, 500)
                    break
                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            setTimeout(() => {
                this.globals.send({
                    path: this.token.getDetail,
                    token: 'getDetail',
                    params: { link: params.link, parent_link: params.parent_link }
                })
            }, 50)
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }
}
