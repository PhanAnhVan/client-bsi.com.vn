import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { Globals } from "../../globals";


@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private http: Http, private globals: Globals) { }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.globals.USERS._check().map(res => {
            
            if (!res.skip) {
                this.router.navigate(['/login']);
            } else {
                this.globals.USERS.set(res.data, true);
            }
            return res.skip;
        });
    }
}
