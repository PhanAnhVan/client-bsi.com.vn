Fancybox.bind('[data-fancybox]', {
    Thumbs: false,
    Toolbar: false,

    Image: {
        zoom: false,
        click: false,
        wheel: 'slide'
    }
})

function libraryGallary() {
    var $container = $('.animate-grid .gallary-thumbs')
    $container.isotope({
        filter: '*',
        animationOptions: {
            duration: 750,
            easing: 'linear',
            queue: false
        }
    })
    $('.animate-grid .categories a').click(function () {
        $('.animate-grid .categories .active').removeClass('active')
        $(this).addClass('active')
        var selector = $(this).attr('data-filter')
        $container.isotope({
            filter: selector,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        })
        return false
    })
}

// function scrolltop() {
//     $(window).on('scroll', function () {
//         if ($(this).scrollTop() > 200) {
//             $('.scrollTop').fadeIn(200)
//         } else {
//             $('.scrollTop').fadeOut(200)
//         }
//     })
//     $('.scrollTop').on('click', function () {
//         $('html, body').animate({ scrollTop: 0 }, 1500)
//         return false
//     })
// }

$(document).ready(function () {
    $('body').tooltip({ selector: '[data-toggle=tooltip]' })
})

jQuery(
    (function ($) {
        'use strict'

        $(window).on('scroll', function () {
            // console.log('🚀 ~ file: js.js ~ line 61 ~ event', $(this).sco)
            if ($(this).scrollTop() > 150) {
                $('.header-nav').addClass('header-fixed')
                // if ($('.header-nav').hasClass('header-fixed')) {
                // $('.navbar-nav.position-fixed').addClass('menu-fixed')
                // }
                $('.main-nav').addClass('menu-shrink')
            } else {
                $('.header-nav').removeClass('header-fixed')
                $('.main-nav').removeClass('menu-shrink')
                // $('.navbar-nav.position-fixed').removeClass('menu-fixed')
            }

            // if ($(this).scrollTop() > 0 && innerWidth < 425) {
            //     $('.navbar-nav.position-fixed').addClass('menu-fixed')
            // } else {
            //     $('.navbar-nav.position-fixed').removeClass('menu-fixed')
            // }
        })

        $(function () {
            $(window).on('scroll', function () {
                var scrolled = $(window).scrollTop()
                scrolled > 500 ? $('.go-top').addClass('active') : $('.go-top').removeClass('active')
                // if (scrolled > 500) $('.go-top').addClass('active')
                // if (scrolled < 500) $('.go-top').removeClass('active')
            })
        })
    })(jQuery)
)

// check if window scroll > 150
if (window.scrollY > 150) {
    // add class to header
    // document.querySelector('.header-nav').classList.add('header-fixed')
    // if (document.querySelector('.header-nav').classList.contains('header-fixed')) {
    document.querySelector('.navbar-nav.position-fixed').classList.add('menu-fixed')
} else {
    document.querySelector('.navbar-nav.position-fixed').classList.remove('menu-fixed')

}
